from setuptools import setup


with open("requirements.txt") as f:
    requirements = f.read().splitlines()


setup(name="abioutput",
      description="Python package to ease reading abinit output.",
      install_requires=requirements,
      )
