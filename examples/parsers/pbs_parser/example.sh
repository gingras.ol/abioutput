#!/bin/bash

#PBS -N example
#PBS -l walltime=1:00:00
#PBS -l nodes=1:ppn=12

MPIRUN="mpirun -npernode=12"
EXECUTABLE=abinit
INPUT=/home/felix/Workspace/abilaunch/examples/writers/example.files
LOG=/home/felix/Workspace/abilaunch/examples/writers/example.log
STDERR=/home/felix/Workspace/abilaunch/examples/writers/stderr

module load MPI/Gnu/gcc4.9.2/openmpi/1.8.8

cd $PBS_O_WORKDIR

$MPIRUN $EXECUTABLE < $INPUT > $LOG 2> $STDERR
