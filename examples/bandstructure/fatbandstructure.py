from abioutput import FatBandstructure, HA_TO_EV


path = "odat_0ph_poi_fatbands_FATBANDS_at0001_Cu_is1_l2_m+2"

fatband = FatBandstructure.from_file(path,
                                     fermi_energy=0.37451 * HA_TO_EV)
fatband.plot_fatband(bands=(60, 79), coeff=2, symmetry="none",
                     title="$Cu1-4d_{x^2-y^2}$",
                     ylabel="Energy (eV)")
