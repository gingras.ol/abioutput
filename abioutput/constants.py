from numpy import pi


# Fundamental constants
c = 299792458  # m/s
G = 6.67408e-11  # m^3/kg/s^2
h = 6.626070040e-34  # kg m^2 / s
e = 1.6021766208e-19  # C
m_e = 9.10938356e-31  # kg
k_B = 1.38064852e-23  # kg m / K / s^2
amu = 1.66053904e-27  # kg
mu_0 = 4 * pi * 10 ** (-7)  # kg m / C^2
hbar = h / (2 * pi)
epsilon_0 = 1 / (mu_0 * c ** 2)
mu_B = e * hbar / (2 * m_e)
FINE_STRUCTURE_CONSTANT = mu_0 * e ** 2 * c / (2 * h)
BOHR_RADIUS = hbar / (m_e * c * FINE_STRUCTURE_CONSTANT)  # meters


# conversion factors
JOULES_TO_EV = 1 / e
RYDBERG_TO_JOULES = (FINE_STRUCTURE_CONSTANT ** 2 * m_e * c ** 2 / 2)
RYDBERG_TO_EV = RYDBERG_TO_JOULES * JOULES_TO_EV
HARTREE_TO_JOULES = 2 * RYDBERG_TO_JOULES
HARTREE_TO_EV = HARTREE_TO_JOULES * JOULES_TO_EV
BOHR_TO_ANGSTROM = BOHR_RADIUS * 10 ** 10
BOHR_TO_METERS = BOHR_RADIUS
ANGSTROM_TO_BOHR = 1 / BOHR_TO_ANGSTROM

AMU_TO_ME = amu / m_e
AMU_TO_KG = amu

# aliases
HA_TO_EV = HARTREE_TO_EV
HARTREE_TO_KELVIN = HARTREE_TO_JOULES / k_B
