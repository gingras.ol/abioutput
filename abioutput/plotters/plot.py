from ..bases import BaseUtility
from .curves import Curve, HLine, VLine, FillBetween, Image
from .plot_tools import ToggleLegend, ToggleGrid
from ..utils import TaggedList
import matplotlib
matplotlib.rcParams["toolbar"] = "toolmanager"
import matplotlib.pyplot as plt  # noqa


class Plot(BaseUtility):
    """Class to plot something.
    """
    _loggername = "Plot"

    def __init__(self, **kwargs):
        """Init method.
        """
        super().__init__(**kwargs)
        # init the attributes of the figure and the curves
        self._fig = None
        self._ax = None
        self._twinx = None
        self._bar = None
        self.curves = TaggedList(loglevel=self._logger.level)
        self.curves_twinx = TaggedList(loglevel=self._logger.level)
        self.images = TaggedList(loglevel=self._logger.level)
        self._xtick_labels = None
        self._xtick_rotation = None
        self.vlines = TaggedList(loglevel=self._logger.level)
        self.hlines = TaggedList(loglevel=self._logger.level)
        self.hlines_twinx = TaggedList(loglevel=self._logger.level)
        self.fill_between = TaggedList(loglevel=self._logger.level)
        self.fill_between_twinx = TaggedList(loglevel=self._logger.level)
        self.show_grid = False
        self._xlabel = ""
        self._ylabel = ""
        self._ylabel_twinx = ""
        self._title = ""
        self._colorbarlabel = ""

    def _add_tools_to_fig(self, fig):
        toolmanager = fig.canvas.manager.toolmanager
        toolbar = fig.canvas.manager.toolbar
        # add the ToggleLegend tool
        toolmanager.add_tool("legend", ToggleLegend)
        toolbar.add_tool(toolmanager.get_tool("legend"), "io")
        # add the ToggleGrid tool
        toolmanager.add_tool("togglegrid", ToggleGrid)
        toolbar.add_tool(toolmanager.get_tool("togglegrid"), "io")

    def set_xlabel(self, value):
        self.xlabel = value

    def normalize_by_curve(self, curve_tag, twinx=False):
        """Normalize all curves with the maximal value of a specific curve.

        Parameters
        ----------
        curve_tag : int, str
                    The curve tag of the curve to which all curves will be
                    normalized.
        twinx : bool, optional
                If True, the normalization procedure is done on the twin axis.
        """
        # find maximum
        if not twinx:
            max_ = self.curves[curve_tag].max
        else:
            max_ = self.curves_twinx[curve_tag].max
        self.normalize_by_factor(max_, twinx=twinx)

    def normalize_by_factor(self, factor, twinx=False):
        """Normalize all curves by a specific factor.

        Parameters
        ----------
        factor : The normalizing factor.
        twinx : bool, optional
                If True, the normalization procedure is done on the twin axis.
        """
        if not twinx:
            curves_to_normalize = self.curves
        else:
            curves_to_normalize = self.curves_twinx
        for curve in curves_to_normalize:
            curve /= factor

    def normalize_by_global_max(self, twinx=False):
        """Normalize all curves by the alltime maximum of all curves.

        Parameters
        ----------
        twinx : bool, optional
                If True, the normalization procedure is done on the twin axis.
        """
        # find max of all curves
        if not twinx:
            curves = self.curves
        else:
            curves = self.curves_twinx
        maximums = [curve.max for curve in curves]
        self.normalize_by_factor(max(maximums), twinx=twinx)

    @property
    def xlabel(self):
        return self._xlabel

    @xlabel.setter
    def xlabel(self, label):
        if not isinstance(label, str):
            raise TypeError("Label must be str.")
        self._xlabel = label

    @property
    def ylabel(self):
        return self._ylabel

    def set_ylabel(self, label):
        self.ylabel = label

    @ylabel.setter
    def ylabel(self, label):
        if not isinstance(label, str):
            raise TypeError("Label must be str.")
        self._ylabel = label

    @property
    def ylabel_twinx(self):
        return self._ylabel_twinx

    @ylabel_twinx.setter
    def ylabel_twinx(self, ylabel):
        self._ylabel_twinx = ylabel

    @property
    def colorbarlabel(self):
        return self._colorbarlabel

    @colorbarlabel.setter
    def colorbarlabel(self, label):
        self._colorbarlabel = label

    @property
    def title(self):
        return self._title

    def set_title(self, title):
        self.title = title

    @title.setter
    def title(self, title):
        if not isinstance(title, str):
            raise TypeError("Title must be str.")
        self._title = title

    @property
    def xtick_labels(self):
        return self._xtick_labels

    def set_xtick_labels(self, labels):
        self.xtick_labels = labels

    @xtick_labels.setter
    def xtick_labels(self, labels):
        # must be a list of two component lists.
        if type(labels) not in (list, tuple):
            raise TypeError("Labels should be a list.")
        # if not an empty list, check that each labels are two components
        if len(labels):
            if type(labels[0]) not in (list, tuple) or len(labels[0]) != 2:
                raise TypeError("Each xtick_labels componant is a list of"
                                " (pos, label).")
        self._xtick_labels = labels

    @property
    def xtick_rotation(self):
        return self._xtick_rotation

    @xtick_rotation.setter
    def xtick_rotation(self, rotation):
        self._xtick_rotation = rotation

    def add_image(self, *args, tag=None, **kwargs):
        """Add a pcolormesh to the plot.

        All parameters except the tag are passed to the Image class.

        Parameters
        ----------
        tag : str, int, optional
              To specify a tag for the image in order to find it more quickly.
              If None, the tag is the index of the image in the list of images.
        """
        self.images.append(Image(*args, **kwargs), tag=tag)

    def add_curve(self, *args, tag=None, twinx=False, **kwargs):
        """Add a curve to the plot.

        All parameters except tag are passed to the Curve class.

        Parameters
        ----------
        tag : str, int, optional
              To specify a tag for the curve in order to find it more quickly.
              If None, the tag is the index of the curve in the list of curves.
        twinx : bool, optional
                If True, the curves is added to the twin axis curves instead
                of the main axis.
        """
        curve = Curve(*args, **kwargs)
        if not twinx:
            self.curves.append(curve, tag=tag)
        else:
            self.curves_twinx.append(curve, tag=tag)

    def remove_curve(self, tag, twinx=False):
        """Removes a curve according to its tag (or index).

        Parameters
        ----------
        tag : str
              The curve tag or index to remove.
        twinx : bool, optional
                If True, the curve is removed from the twin axis instead
                of the main axis.
        """
        if not twinx:
            self.curves.remove(tag)
        else:
            self.curves_twinx.remove(tag)

    def clear_curve_labels(self, twinx=False):
        """Sets the labels of all curves to None.

        Parameters
        ----------
        twinx : bool, optional
                If True, the label clearing is done on the twin axis curves.
        """
        if twinx:
            curves = self.curves_twinx
        else:
            curves = self.curves
        for curve in curves:
            curve.clear_label()

    def add_fill_between(self, *args, tag=None, twinx=False, **kwargs):
        """Add a fill between curve to the plot.

        All parameters except tag are passed to the FillBetween class.

        Parameters
        ----------
        tag : optional
              To specify a tag for the fill_between curve to fint it quickly.
              If None, the tag is simply the normal index.
        twinx : bool, optional
                If True, the fill_between curve is added to the twin axis.
        """
        fill_between = FillBetween(*args, **kwargs)
        if not twinx:
            self.fill_between.append(fill_between, tag=tag)
        else:
            self.fill_between_twinx.append(fill_between, tag=tag)

    def remove_fill_between(self, tag, twinx=False):
        """Remove a fill_between curve according to its tag.

        Parameters
        ----------
        tag : the tag/index of the fill_between to remove.
        twinx : bool, optional
                If True, the removal is done on the twin axis fill_betweens.
        """
        if not twinx:
            self.fill_between.remove(tag)
        else:
            self.fill_between_twinx.remove(tag)

    def add_vline(self, *args, tag=None, **kwargs):
        """Add a vertical line.

        All parameters except tag are passed to the VLine class.

        Parameters
        ----------
        tag : optional
              Specifies a tag for the vline to find it quickly.
              If None, the tag is simply the normal index.
        """
        vline = VLine(*args, **kwargs)
        self.vlines.append(vline, tag=tag)

    def remove_vline(self, tag):
        """Remove vline according to its tag or index.
        """
        self.vlines.remove(tag)

    def add_hline(self, *args, tag=None, twinx=False, **kwargs):
        """Add a horizontal line.

        All parameters except tag are passed to the HLine class.

        Parameters
        ----------
        tag : optional
              Specifies a tag for the hline to find it quickly.
              If None, the tag is simply the normal index.
        twinx : bool, optional
                If True, the horizontal line is added on the twin axis
                instead of the main axis.
        """
        hline = HLine(*args, **kwargs)
        if not twinx:
            self.hlines.append(hline, tag=tag)
        else:
            self.hlines_twinx.append(hline, tag=tag)

    def remove_hline(self, tag, twinx=False):
        """Remove a hline according to its tag or index.

        Parameters
        ----------
        tag : tag/index of the hline to remove.
        twinx : bool, optional
                If True, the removal is done on the twin axis instead of
                the main one.
        """
        if not twinx:
            self.hlines.remove(tag)
        else:
            self.hlines_twinx.remove(tag)

    def set_curve_label(self, label, tag, twinx=False):
        """Set the label of a specific curve.

        Parameters
        ----------
        label : str
                The label to apply to the curve.
        tag : The curve tag/index.
        twinx : bool, optional
                If True, the label is applied on the twin axis.
        """
        if not twinx:
            self.curves[tag].label = label
        else:
            self.curves_twinx[tag].label = label

    def __add__(self, plot):
        """Method that allows the addition of two plots.

        The addition is defined as another plot with the same characteritics
        but the superposition of all curves.
        """
        if self.xlabel != plot.xlabel or (self.ylabel != plot.ylabel or
                                          self.ylabel_twinx !=
                                          plot.ylabel_twinx):
            raise ValueError("Axis labels does not match :(")
        newplot = Plot()
        for attr in ("fill_between", "curves", "hlines", "vlines", "images",
                     "fill_between_twinx", "curves_twinx", "hlines_twinx"):
            setattr(newplot, attr, getattr(self, attr) + getattr(plot, attr))
        if self.xtick_labels is not None and plot.xtick_labels is not None:
            all_xticks = list(self.xtick_labels)
            for xtick in plot.xtick_labels:
                if xtick not in all_xticks:
                    all_xticks.append(xtick)
            newplot.xtick_labels = all_xticks
        elif self.xtick_labels is None and plot.xtick_labels is not None:
            newplot.xtick_labels = plot.xtick_labels
        elif self.xtick_labels is not None and plot.xtick_labels is None:
            newplot.xtick_labels = self.xtick_labels
        # else do nothing
        # compute new title
        if self.title == "" and plot.title != "":
            title = plot.title
        elif self.title != "" and plot.title == "":
            title = self.title
        elif self.title == "" and plot.title == "":
            title = ""
        else:
            title = self.title + " + " + plot.title
        newplot.title = title
        # grid, show if one of the plots is set to shot it
        newplot.show_grid = self.show_grid or plot.show_grid
        return newplot

    def plot_on_axis(self, fig, axis, **kwargs):
        """Same as plot, but give an axis and a figure Matplotlib object.
        All other kwargs go to the plot method.
        """
        self._fig = fig
        self._ax = axis
        self.plot(**kwargs)

    @property
    def _need_twinx(self):
        if len(self.curves_twinx) or len(self.fill_between_twinx):
            return True
        return False

    def plot(self,
             legend_outside=False,
             show_legend=True,
             show=True,
             ax=None):
        """Method to plot the figure.

        Parameters
        ----------
        show_legend : bool, optional
                      If True, the legend is shown.
        legend_outside : bool, optional
                         If True, the legend (if displayed) will be drawn
                         outside graph.
        show : bool, optional
               If False, the plot is not shown but it is stored in an internal
               attribute.
        """
        if self._fig is None and self._ax is None:
            self._fig = plt.figure()
            self._ax = self._fig.add_subplot(111)
            if self._need_twinx:
                self._twinx = self._ax.twinx()
        # plot curves and lines
        for curve in self.curves:
            curve.plot_on_axis(self._ax)
        for fill in self.fill_between:
            fill.plot_on_axis(self._ax)
        for hline in self.hlines:
            hline.plot_on_axis(self._ax)
        for vline in self.vlines:
            vline.plot_on_axis(self._ax)
        if self._need_twinx:
            for fill in self.fill_between_twinx:
                fill.plot_on_axis(self._twinx)
            for hline in self.hlines_twinx:
                hline.plot_on_axis(self._twinx)
            for curve in self.curves_twinx:
                curve.plot_on_axis(self._twinx)
            self._twinx.set_ylabel(self.ylabel_twinx)
        # plot image
        ax_images = []
        for image in self.images:
            ax_images.append(image.plot_on_axis(self._ax))
        # set ticks
        if self.xtick_labels is not None:
            ticks_loc = [x[0] for x in self.xtick_labels]
            ticks_labels = [x[1] for x in self.xtick_labels]
            self._ax.set_xticks(ticks_loc)
            self._ax.set_xticklabels(ticks_labels)
        if self.xtick_rotation is not None:
            for tick in self._ax.get_xticklabels():
                tick.set_rotation(self.xtick_rotation)
        # set title and axis labels
        self._ax.set_title(self.title)
        self._ax.set_xlabel(self.xlabel)
        self._ax.set_ylabel(self.ylabel)
        # show legend if needed and if at least one curve_labels is not None
        lines, labels = self._ax.get_legend_handles_labels()
        if self._need_twinx:
            lines2, labels2 = self._twinx.get_legend_handles_labels()
            lines += lines2
            labels += labels2
        if show_legend and labels:
            if legend_outside:
                self._ax.legend(lines, labels, loc="center left",
                                bbox_to_anchor=(1, 0.5))
            else:
                self._ax.legend(lines, labels, loc="best")
        if len(self.images):
            if len(self.images) > 1:
                self._logger.warning("Not sure more than 1 images works...")
            for ax_image in ax_images:
                self._bar = self._fig.colorbar(ax_image, ax=self._ax)
                self._bar.set_label(self.colorbarlabel)
        if self.show_grid:
            self._ax.grid(True)
        # before showing, add tools
        self._add_tools_to_fig(self._fig)
        if show:
            plt.show()

    def save(self, path):
        if self._fig is None:
            raise ValueError("Use the plot method to generate figure.")
        self._fig.savefig(path)

    def reset(self):
        del self._fig
        self._fig = None

    def show(self, *args, **kwargs):
        # just an alias whenever user is confused
        return self.plot(*args, **kwargs)
