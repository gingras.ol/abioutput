from ..bases import BaseUtility
from .plot import Plot
import matplotlib.pyplot as plt
import numpy as np


class MultiPlot(BaseUtility):
    """Class to make a single plot with subplots.
    """
    _loggername = "MultiPlot"

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self._fig = None
        self.plot_rows = [[]]

    def add_plot(self, plot, row):
        """Add plot on a row.

        Parameters
        ----------
        plot : Plot instance
        row : int
              The row index to add the plot.
        """
        if not isinstance(plot, Plot):
            raise TypeError("Argument must be a Plot instance.")
        while row + 1 > len(self.plot_rows):
            self.plot_rows.append([])
        self.plot_rows[row].append(plot)

    @property
    def plots(self):
        allplots = []
        for row in self.plot_rows:
            allplots += row
        return allplots

    def _check_rows_equal(self):
        length = len(self.plot_rows[0])
        for row in self.plot_rows[1:]:
            if len(row) != length:
                raise ValueError("Not all rows are equals!")

    def _sanitize_show_legend_on(self, show_legend_on):
        if show_legend_on is None:
            return []
        arr = np.array(show_legend_on)
        if len(arr.shape) == 1 and arr.shape[0] == 2:
            # only one coordinate
            show_legend_on = [tuple(show_legend_on)]
        elif len(arr.shape) != 2 or (len(arr.shape) == 2 and
                                     arr.shape[-1] != 2):
            raise ValueError(f"show_legend_on: {show_legend_on} invalid shape")
        # else return the list of tuples
        # but make sure indices are good
        new = []
        n_rows = len(self.plot_rows)
        # we assume here that all rows are equal
        n_columns = len(self.plot_rows[0])
        for coord in show_legend_on:
            x = coord[0]
            y = coord[1]
            while x < 0:
                x += n_rows
            while y < 0:
                y += n_columns
            if x > n_rows:
                raise ValueError("Cannot ask to show legend"
                                 " further than n_rows!")
            if y > n_columns:
                raise ValueError("Cannot ask to show legend"
                                 " further than n_columns!")
            new.append((x, y))
        return new

    def plot(self, show=True, show_legend_on=None):
        """Plot the multiplot.

        Parameters
        ----------
        show: bool, optional
              If True, the plos is shown.
        show_legend_on: list, optional
                        The list of subplots to show the legend.
                        It's a list of tuples with the coordinates being the
                        ('row number', 'column number')
        """
        self._check_rows_equal()
        self._fig = plt.figure()
        show_legend_on = self._sanitize_show_legend_on(show_legend_on)

        n_rows = len(self.plot_rows)
        n_columns = len(self.plot_rows[0])

        for i, row in enumerate(self.plot_rows):
            for j, plot in enumerate(row):
                ax = self._fig.add_subplot(n_rows, n_columns,
                                           n_rows * i + j + 1)
                legend = False
                if (i, j) in show_legend_on:
                    legend = True
                plot.plot_on_axis(self._fig, ax, show=False,
                                  show_legend=legend)
        if show:
            plt.show()

    def save(self, path):
        if self._fig is None:
            raise ValueError("Run the plot method to create "
                             "the figure before saving it.")
        self._fig.savefig(path)
