from ..routines import sort_data as fsort
from ..bases import BaseUtility
import abc
import numpy as np


class BaseCurve(BaseUtility, abc.ABC):
    """Base class for curves.
    """
    def _check_list(self, data):
        # check if arguments are array-like and return numpy arrays
        if type(data) not in (np.ndarray, list, tuple):
            raise TypeError("Argument should be array-like.")
        if isinstance(data, np.ndarray):
            if len(data.shape) >= 2:
                raise TypeError("Argument is > 1D data!")
        return np.array(data, dtype=float)  # transform all to floats

    @abc.abstractmethod
    def plot_on_axis(self, *args, **kwargs):
        # must be overidden
        pass


class Image(BaseUtility):
    """Class that represents an image in order to plot it on an axis.
    """
    _loggername = "Image"

    def __init__(self, xdata, ydata, zdata, cmap="hot", interpolation="none",
                 **kwargs):
        super().__init__(**kwargs)
        self._xdata = None
        self._ydata = None
        self._zdata = None
        self.cmap = cmap
        self.interpolation = interpolation
        self.xdata, self.ydata = xdata, ydata
        self.zdata = zdata

    @property
    def xdata(self):
        return self._xdata

    @xdata.setter
    def xdata(self, xdata):
        self._xdata = np.array(xdata)

    @property
    def ydata(self):
        return self._ydata

    @ydata.setter
    def ydata(self, ydata):
        self._ydata = np.array(ydata)

    @property
    def zdata(self):
        return self._zdata

    @zdata.setter
    def zdata(self, zdata):
        self._zdata = np.array(zdata)

    def plot_on_axis(self, axis):
        return axis.imshow(self.zdata, cmap=self.cmap, origin="lower",
                           extent=[self.xdata[0], self.xdata[-1],
                                   self.ydata[0], self.ydata[-1]],
                           interpolation=self.interpolation)


class Curve(BaseCurve):
    """Class that represents a curve."""
    _loggername = "Curve"

    def __init__(self, xdata, ydata, linestyle="-", color="k", label=None,
                 linewidth=1.0, normalize=False, semilogy=False,
                 semilogx=False, loglog=False, marker=None,
                 sort_data=False, **kwargs):
        """Curve init method.

        Parameters
        ----------
        xdata : array-like
                 x-data points.
        ydata : array-like
                 y-data points.
        linestyle : str, optional
                    Curve linestyle.
        color : str, optional
                Curve color.
        label : str, optional
                Curve label. None = no label.
        linewidth : float, optional
                    Sets the width of the curves.
        normalize : bool, optional
                    If True, the ydata is normalized by its maximum value.
        semilogy : bool, optional
                   If True, the ydata is plot on a semilog axis instead of a
                   normal axis.
        semilogx : bool, optional
                   Same as semilogy but for xaxis.
        loglog : bool, optional
                 Same as semilogy but for both axis. If semilogx and semilogy
                 are both True at the same time: loglog will be set to True.
        marker : str, optional
                 The marker for the ydata. If None, no marker is applied.
        sort_data : bool, optional
                    If True, xdata will be sorted and ydata too according to
                    the sorting of xdata.
        """
        super().__init__(**kwargs)
        self.xdata = self._check_list(xdata)
        self.ydata = self._check_list(ydata)
        if len(self.xdata) != len(self.ydata):
            raise ValueError("xdata and ydata must be same length!")
        if sort_data:
            self.xdata, self.ydata = fsort(self.xdata, self.ydata)
        if linestyle is None:
            # matplotlib only accepts strings...
            linestyle = "None"
        self.linestyle = linestyle
        self.color = color
        self.label = label
        self.linewidth = linewidth
        self.semilogy = semilogy
        self.semilogx = semilogx
        self.loglog = loglog
        if self.semilogx and self.semilogy:
            self.loglog = True
        self.marker = marker
        if normalize:
            self.ydata = self.ydata / self.max

    def clear_label(self):
        """Sets the curve label to None.
        """
        self.label = None

    def plot_on_axis(self, axis):
        args = [self.xdata,
                self.ydata]
        kwargs = {"linestyle": self.linestyle,
                  "color": self.color,
                  "label": self.label,
                  "linewidth": self.linewidth,
                  "marker": self.marker}
        if self.loglog:
            # important to call this first because if semilogx-y are both True,
            # loglog wil be set to True
            return axis.loglog(*args, **kwargs)
        if self.semilogy:
            return axis.semilogy(*args, **kwargs)
        if self.semilogx:
            return axis.semilogx(*args, **kwargs)
        return axis.plot(*args, **kwargs)

    @property
    def wheremax(self):
        return self.xdata[np.where(self.ydata == self.max)[0]]

    @property
    def wheremin(self):
        return self.xdata[np.where(self.ydata == self.min)[0]]

    @property
    def max(self):
        return np.max(self.ydata)

    @property
    def min(self):
        return np.min(self.ydata)

    def __truediv__(self, value):
        self._check_scalar(value)
        self.ydata /= value
        return self

    def __add__(self, value):
        self._check_scalar(value)
        self.ydata += value
        return self

    def __mul__(self, value):
        self._check_scalar(value)
        self.ydata *= value
        return self

    def __sub__(self, value):
        self._check_scalar(value)
        self.ydata -= value
        return self

    def _check_scalar(self, value):
        if not isinstance(value, int) and not isinstance(value, float):
            raise TypeError("Arithmetic operations only with int or floats.")


class FillBetween(BaseCurve):
    """Class that represents a filled area between two curves.
    """
    _loggername = "FillBetween"

    def __init__(self, xdata, ydata1, ydata2, color="r", **kwargs):
        """FillBetween area init method.

        Parameters
        ----------
        xdata : array-like
                x-data points.
        ydata1, ydata2 : array-like
                         The y coordinates of the two curves to fill between.
        color : str, optional
                The color of the filled area.
        """
        super().__init__(**kwargs)
        self.xdata = self._check_list(xdata)
        self.ydata1 = self._check_list(ydata1)
        self.ydata2 = self._check_list(ydata2)
        self.color = color

    def plot_on_axis(self, axis):
        return axis.fill_between(self.xdata,
                                 self.ydata1,
                                 self.ydata2,
                                 color=self.color)


class StraightLine(BaseUtility, abc.ABC):
    def __init__(self, pos, linestyle="-", color="k", linewidth=1.0,
                 label=None, **kwargs):
        super().__init__(**kwargs)
        self.position = pos
        self.linestyle = linestyle
        self.color = color
        self.linewidth = linewidth
        self.label = label

    @abc.abstractmethod
    def plot_on_axis(self, *args, **kwargs):
        pass


class VLine(StraightLine):
    _loggername = "VLine"

    def plot_on_axis(self, axis):
        return axis.axvline(self.position,
                            linestyle=self.linestyle,
                            color=self.color,
                            linewidth=self.linewidth,
                            label=self.label)


class HLine(StraightLine):
    _loggername = "HLine"

    def plot_on_axis(self, axis):
        return axis.axhline(self.position,
                            linestyle=self.linestyle,
                            color=self.color,
                            linewidth=self.linewidth,
                            label=self.label)
