from matplotlib.backend_tools import ToolToggleBase
import os


IMAGES_DIR = os.path.join(os.path.dirname(__file__), "tool_buttons_images")


class BaseCustomToggleTool(ToolToggleBase):
    """Just for rebasing same methods.
    """
    def enable(self, event):
        self.visibility(True)

    def disable(self, event):
        self.visibility(False)


# Taken from:
# https://github.com/fariza/pycon2017/blob/master/toolmanager.pdf
class ToggleLegend(BaseCustomToggleTool):
    description = "Turn on/off the legend."
    default_toggled = True
    image = os.path.join(IMAGES_DIR, "toggle_legend.png")

    def visibility(self, state):
        for leg in list(self.figure.legends):
            leg.set_visible(state)
        for a in self.figure.get_axes():
            leg = a.get_legend()
            if leg:
                leg.set_visible(state)
        self.figure.canvas.draw_idle()


class ToggleGrid(BaseCustomToggleTool):
    description = "Turn on/off grid."
    default_toggled = False
    image = os.path.join(IMAGES_DIR, "toggle_grid.png")

    def visibility(self, state):
        # set the grid on the main axis
        self.figure.axes[0].grid(b=state)
        self.figure.canvas.draw_idle()
