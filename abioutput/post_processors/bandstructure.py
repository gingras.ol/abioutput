from abioutput import EIGParser, QEFreqParser
from abioutput.plotters import Plot
from abilaunch.routines import is_vector, is_2d_arr, is_scalar_or_str
from ..bases import BaseUtility
import numpy as np


HIGH_SYM_PTS = {"fcc": {"L": (0.5, 0.5, 0.5),
                        r"$\Gamma$": (0.0, 0.0, 0.0),
                        "X": (0.0, 0.5, 0.5),
                        "$X_2$": (1.0, 0.0, 0.0),  # can be considered as X
                        "W": (0.25, 0.75, 0.5),
                        "U": (0.25, 0.625, 0.625),
                        "K": (0.375, 0.75, 0.375),
                        },
                "none": {},
                "rectangle": {r"$\Gamma$": (0.0, 0.0, 0.0),
                              "X": (0.5, 0.0, 0.0),
                              "Y": (0.0, 0.5, 0.0),
                              "M": (0.5, 0.5, 0.0)},
                "hexagonal": {r"$\Gamma$": (0.0, 0.0, 0.0),
                              "M": (0.5, 0.0, 0.0),
                              "K": (0.6667, 0.3333, 0.0),
                              "A": (0.0, 0.0, 0.5),
                              "L": (0.5, 0.0, 0.5),
                              "H": (0.6667, 0.3333, 0.5),
                              },
                "tetragonal": {r"$\Gamma$": (0.0, 0.0, 0.0),
                               "M": (0.5, 0.5, 0.0),
                               "X": (0.5, 0.0, 0.0),
                               "Z": (0.0, 0.0, 0.5),
                               "R": (0.5, 0.0, 0.5),
                               "A": (0.5, 0.5, 0.5),
                               },
                }


class BandStructure(BaseUtility):
    """Class that represents a Band Structure.
    """
    _loggername = "BandStructure"

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self._kpts = None
        self._nkpts = None
        self._energies = None
        self._fermiband = None
        self._fermienergy = None
        self._bands = None
        self._nbands = None

    @property
    def fermi_energy(self):
        if self._fermienergy is not None:
            return self._fermienergy
        raise ValueError("Set fermi_energy before using it.")

    @fermi_energy.setter
    def fermi_energy(self, fermi_energy):
        if not is_scalar_or_str(fermi_energy):
            raise TypeError(f"Expected scalar but got: {fermi_energy}")
        if self.fermi_band is not None:
            self._logger.warning("Specified fermi_band already. Manually "
                                 "overwriting fermi_energy.")
            self._fermiband = None
        self._fermienergy = float(fermi_energy)

    @property
    def fermi_band(self):
        if self._fermiband is not None:
            return self._fermiband
        raise ValueError("Set fermi_band before using it.")

    @fermi_band.setter
    def fermi_band(self, fermi_band):
        if not isinstance(fermi_band, int):
            raise TypeError(f"Expected an integer but got: {fermi_band}")
        if self._fermienergy is not None:
            self._logger.warning("Fermi energy was already set. Overwritting "
                                 " existing value using the fermi band index.")
        self.fermi_energy = max(self.bands[fermi_band])
        self._fermiband = fermi_band

    @property
    def kpts(self):
        if self._kpts is not None:
            return self._kpts
        raise ValueError("Need to set kpts/qpts before using them.")

    @kpts.setter
    def kpts(self, kpts):
        # kpts must be a Nkpts x 3 array / list
        if is_vector(kpts):
            kpts = [kpts]
        if not is_2d_arr(kpts):
            raise TypeError(f"Expected 2d array but got: {kpts}")
        kpts = np.array(kpts)
        if kpts.shape[-1] != 3:
            raise TypeError(f"Expected Nx3 array but got: {kpts}")
        self._kpts = kpts
        self._nkpts = len(self.kpts)
        # check nkpts vs number of pts for energies
        try:
            # if energies not set it will raise an error
            if self.energies.shape[0] != self.nkpts:
                raise TypeError(f"npts ({self.nkpts}) not commensurable with "
                                f"energies ({self.energies.shape[0]})")
        except ValueError:
            # energies not set, bypass the check
            pass

    @property
    def nkpts(self):
        if self._nkpts is not None:
            return self._nkpts
        raise ValueError("Need to set kpts/qpts before getting nb of pts.")

    @property
    def qpts(self):
        # samething as kpts
        return self.kpts

    @qpts.setter
    def qpts(self, qpts):
        # use kpts for qpts
        self.kpts = qpts

    @property
    def nqpts(self):
        return self.nkpts

    @property
    def energies(self):
        if self._energies is not None:
            return self._energies
        raise ValueError("Need to set energies before using them.")

    @energies.setter
    def energies(self, energies):
        if is_vector(energies):
            # only one point
            energies = [energies]
        if not is_2d_arr(energies):
            raise ValueError(f"Expected 2D array but got: {energies}")
        energies = np.array(energies)
        # check size with number of kpts
        try:
            # if kpts aren't set, this will raise an error
            if self.energies.shape[0] != self.nkpts:
                raise TypeError(f"nb of energies ({self.energies.shape[0]}) "
                                f" not commensurable with number of points "
                                f"({self.nkpts}).")
        except ValueError:
            # bypass check
            pass
        self._energies = energies
        # set nkpts
        self._nkpts = energies.shape[0]
        # set nbands
        self._nbands = energies.shape[1]

    @property
    def bands(self):
        return self.energies.T

    @property
    def nbands(self):
        if self._nbands is not None:
            return self._nbands
        raise ValueError("Set energies to get nbands")

    def _get_sym_pts_labels(self, symmetry):
        labels = []
        labels_loc = []
        high_sym_coordinates = HIGH_SYM_PTS[symmetry]
        for index, kpt in enumerate(self.kpts):
            for label, high_sym_kpt in high_sym_coordinates.items():
                # round the kpt coordinates to 4th decimal
                kpt = [round(x, 4) for x in kpt]
                high_sym_kpt = list(high_sym_kpt)
                if kpt == high_sym_kpt:
                    labels_loc.append(index)
                    if label == "Gamma":
                        labels.append("$\Gamma$")
                    else:
                        labels.append(label)
                    break
        return labels, labels_loc

    def _get_considered_bands(self, array, bands):
        """Returns the considered bands of an array if needed.
        """
        considered_bands = array
        if bands is not None:
            considered_bands = array[range(bands[0], bands[1] + 1), :]
        return considered_bands

    def plot(self, bands=None, symmetry="none",
             line_at_zero=True,
             high_sym_vlines=True,
             other_k_labels=None,
             ylabel="Energy",
             yunits=None,
             title="",
             save_at=None, show=True,
             color="k",
             linestyle="-",
             show_bandgap=False):
        """Plot the bandstructure.

        Parameters
        ----------
        symmetry: str, optional, {"none", "rectangle", "FCC"}
                  Gives the crystal symmetry of the structure. This will
                  able the labelling of the high symmetry points in the
                  Brillouin Zone.
        line_at_zero: bool, optional
                      If True, a line is drawn at 0 energy.
        bands: list, optional
               Selects the range of bands to plot (starting from 0).
               If set to None, all bands are shown.
        color : str, optional
                Band curve colors.
        linestyle : str, optional
                    Band curve linestyles.
        high_sym_vlines : bool, optional
                          If True, plain vertical lines will be shown
                          at the high symetry points.
        save_at: str, optional
                 If not None, gives the path to where the figure will be saved.
        show: bool, optional
              If True, the plot will be shown.
        ylabel : str, optional
                 ylabel for the plot.
        yunits : str, optional
                 If not None, states the units of the y axis. They will be
                 added to the plot.
        title : str, optional
                The plot's title.
        show_bandgap : bool, optional
                       If True, the band gap value will be shown in the title.
        """
        considered_bands = self._get_considered_bands(self.bands, bands)
        try:
            fermi_energy = self.fermi_energy
        except ValueError:
            self._logger.warning("Fermi energy not set, assuming it is 0...")
            fermi_energy = 0
        ys = considered_bands - fermi_energy
        xs = list(range(len(self.kpts)))
        labels, labels_loc = self._get_sym_pts_labels(symmetry)

        plot = Plot()
        plot.ylabel = ylabel
        if yunits is not None:
            plot.ylabel += f" [{yunits}]"
        plot.title = title
        if show_bandgap:
            if self._fermiband is None:
                raise ValueError(f"Cannot compute gap if fermi"
                                 f" band not given...")
            gap = min(self.bands[self.fermi_band + 1]) - self.fermi_energy
            plot.title = f"$E_g={gap:.3f}$"
        plot.xtick_labels = [(pos, label) for pos, label in
                             zip(labels_loc, labels)]
        if line_at_zero:
            plot.add_hline(0, linestyle="--")
        if high_sym_vlines:
            for pos in labels_loc:
                plot.add_vline(pos)

        for band in ys:
            plot.add_curve(xs, band, color=color, linestyle=linestyle)
        # show or save now if needed.
        self._show_save_fig(plot, show, save_at)
        return plot

    def _show_save_fig(self, plot, show, save_at):
        if show or save_at is not None:
            plot.plot()
        if save_at is not None:
            plot.save(save_at)

    @classmethod
    def from_EIG(cls, path, **kwargs):
        """Classmethod to read kpts and eigenvalues directly from an
        Abinit EIG file.

        Sets the kpts and the energies but still need to set fermi_energy
        or fermi_band manually after that.

        Parameters
        ----------
        path : The EIG file path.
        """
        bandstructure = cls(**kwargs)
        eigparser = EIGParser.from_file(path,
                                        loglevel=bandstructure._logger.level)
        eigs = np.array(eigparser.data["eigenvalues"])
        bandstructure.kpts = eigparser.data["coordinates"]
        bandstructure.energies = eigs
        return bandstructure

    @classmethod
    def from_freq(cls, path, **kwargs):
        """Class method to read kpts/qpts and energies directly from a QE
        freq file.

        Sets the kpts/qpts and the energies but still need to set fermi_energy
        or fermi_band manually if necessary after that.

        Parameters
        ----------
        path : The .freq file path.
        """
        bandstructure = cls(**kwargs)
        freqparser = QEFreqParser(path, loglevel=bandstructure._logger.level)
        kpts = np.array(freqparser["coordinates"])
        eigs = np.array(freqparser["eigenvalues"])
        bandstructure.kpts = kpts
        bandstructure.energies = eigs
        return bandstructure
