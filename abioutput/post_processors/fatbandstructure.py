import numpy as np
from abioutput import FatbandParser
from .bandstructure import BandStructure


# FIXME: this shall not work anymore because of changes in Bandstructure...
class FatBandstructure(BandStructure):
    """Object that handles a fat band structure given a list of kpts,
    eigenvalues and characters. Can also read directly the data from a file
    using the from_file classmethod.

    Parameters
    ----------
    kpts : list of kpts. Can be set to None => a list of zeros will be used.
    eigenvalues : list of eigenvalues for each kpts and each band.
    characters: list of characters for each kpts and each band.
    other kwargs are passed to the base class.
    """

    def __init__(self, kpts, eigenvalues, characters, **kwargs):
        self.characters = characters
        self.fat_bands = self._get_bands(self.characters)
        self._has_coordinates = True
        if kpts is None:
            kpts = np.zeros((eigenvalues.shape[0], 3))
            self._has_coordinates = False
        super().__init__(kpts, eigenvalues, **kwargs)

    def plot_fatband(self, *args, coeff=None, fatbandcolor="r", **kwargs):
        """Plot the fat band structure. All arguments are passed to the
        plot method which plots the band structure.

        Parameters
        ----------
        coeff: float, optional
               Divide the width of the fat band by this number if not None.
        fatbandcolor: str, optional
                      The color of the fat band.
        All other args and kwargs are passed to the plot method to generate
        the band structure.
        """
        bands = kwargs.get("bands", None)
        symmetry = kwargs.pop("symmetry", "none")
        # intercept show or save at before completing the graph
        show = kwargs.pop("show", True)
        save_at = kwargs.pop("save_at", None)
        if not self._has_coordinates:
            # if user didn't give coordinates, don't use symmetries!
            symmetry = "none"
        plot = self.plot(*args, show=False,
                         save_at=None,
                         symmetry=symmetry,
                         **kwargs)
        # add fat bands
        considered_bands = self._get_considered_bands(self.characters, bands)
        xs = plot.curves[0].xdata
        # compute the averages of chars bands over bands for regularization
        for curve, char_width in zip(plot.curves, considered_bands):
            eigline = curve.ydata
            if coeff is not None:
                char_width /= coeff
            lowerline = eigline - char_width
            higherline = eigline + char_width
            plot.add_fill_between(xs, lowerline, higherline,
                                  color=fatbandcolor)
        self._show_save_fig(plot, show, save_at)
        return plot

    @classmethod
    def from_file(cls, path, conversion_factor=None, coordinates=None,
                  **kwargs):
        """Create a FatBandstructure directly from a file.

        Notes
        -----
        Beware that the FATBAND file written by abinit does not state the
        kpts coordinates. You can manually insert them via the coordinates
        optional arguments. Else, no symmetry lines will be printed.

        Parameters
        ----------
        path : the path to the fat band file.
        conversion_factor : float, optional
                            Multiply all the eigenvalues with this factor.
        coordinates : list, optional
                      The list of coordinates of each kpts. The dimensions
                      must match the eigenvalues and the characters.
        Other kwargs are passed directly to the init method.
        """
        parser = FatbandParser(path)
        eigs = parser.data["eigenvalues"].T
        chars = parser.data["characters"]
        if conversion_factor is not None:
            eigs *= conversion_factor
        return cls(coordinates, eigs, chars, **kwargs)
