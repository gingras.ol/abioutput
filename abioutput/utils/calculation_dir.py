from .bases import BaseBuilder
from .routines import search_in_all_subdirs
from .checkers import StatusChecker
from abioutput.parsers import (AbinitFilesFileParser, OpticFilesFileParser,
                               AbinitOutputParser, AbinitLogParser,
                               QELogParser)
import os


# TODO: CLEAN THIS MESS! Need more OOP here...
# To achieve this, will probably need to clean the whole module...


class CalculationDir(BaseBuilder):
    """Class that represents a calculation directory.
    """
    _loggername = "CalculationDir"

    def __init__(self, directory, ignore=None, **kwargs):
        """CalculationDir init method.

        Parameters
        ----------
        directory : str
                    The calculation directory.
        ignore : list, optional
                 If not None, this list of directories will be ignored while
                 searching for the corresponding calculation files.
        """
        super().__init__(directory, **kwargs)
        if not self.is_calculation_dir(directory):
            raise FileNotFoundError(f"No input file found in {directory}.")
        self._ignore = ignore
        # need to check which type of calculation it is
        # just try ordinary file, it it doesnt work, its optic!
        level = self._logger.level
        try:
            filesfile = self._get_files_file(ignore=ignore)
        except ValueError:
            # didnt find any file which ends with .files
            self._type = "qe"
            self._logger.debug(f"{directory} is a QE directory.")
            self.log_file = None
            try:
                self.log_file = self._get_log_file(ignore=ignore)
            except ValueError:
                self._logger.warning("Found calc dir but calc has not started"
                                     f" yet: {directory}")
        else:
            self._logger.debug(f"Files file found at {filesfile}.")
            self._type = "abinit"
            try:
                cls = AbinitFilesFileParser
                self.filesfile = cls(filesfile, loglevel=self._logger.level)
            except IndexError:
                self._type = "optic"
                self._logger.debug("filesfile parser don't work: try optic")
                self.filesfile = OpticFilesFileParser(filesfile,
                                                      loglevel=level)
        self.inputfile = self._get_input_file(ignore=ignore)
        self._outputfile = None

    @property
    def outputfile(self):
        if self._outputfile is not None:
            return self._outputfile
        stat = self.status["calculation_finished"]
        if stat is False or stat == "error":
            raise LookupError("Calculation is not finished or errored,"
                              " cannot analyse output file.")
        if self._type == "abinit":
            cls = AbinitOutputParser
            self._outputfile = cls(self.filesfile["output_path"])
        elif self._type == "qe":
            self._outputfile = QELogParser(self.log_file)
        else:
            # optic calculation: no output file. use log instead
            logfile = self._get_log_file(ignore=self._ignore)
            self._outputfile = AbinitLogParser(logfile)
        return self._outputfile

    @property
    def is_calculation_finished(self):
        return self.status["calculation_finished"]

    @property
    def is_calculation_converged(self):
        # check if computation is finished
        try:
            self.outputfile
        except LookupError:
            self._logger.error("Cannot read convergence if computation"
                               " is not finished.")
            return False
        if self._type == "abinit":
            iscf = self.get_output_var("iscf")
            if iscf < 0:
                # NON SCF CALCULATION => cannot tell if convergence is reached
                raise ValueError(f"iscf={iscf}<0 => cannot tell if"
                                 " conv. is reach")
            ionmov = self.get_output_var("ionmov")
            if ionmov is None:
                ionmov = 0
            if ionmov > 0:
                # ionmov calculation => check for Force convergence
                return self._dig_output_for_convergence(("gradients are"
                                                         " converged"),
                                                        ("not enough Broyd/MD"
                                                         " steps to converge"
                                                         " gradients"))
            # standard GS calculation
            return self._dig_output_for_convergence("converged",
                                                    ("not enough SCF "
                                                     "cycles to converge"))
        elif self._type == "qe":
            return self._dig_output_for_convergence(("convergence has been"
                                                     " achieved"),
                                                    ("convergence NOT"
                                                     " achieved after"))
        else:
            raise NotImplementedError(f"{self._type} not implemented")

    def _dig_output_for_convergence(self, converged_keywords,
                                    nonconverged_keywords):
        if self._type == "abinit":
            filename = self.filesfile["output_path"]
        else:
            filename = self.log_file
        with open(filename) as f:
            lines = f.readlines()
        for line in lines[::-1]:
            if converged_keywords in line:
                return True
            elif nonconverged_keywords in line:
                return False
        # if we are here, computation is not finished => return False
        self._logger.warning("Could not find the convergence status...")
        return False

    def get_output_var(self, outputvar):
        """Returns the value of an output variable from this calculation.

        Parameters
        ----------
        outputvar : str
                    The name of the output variable.
        """
        self._logger.debug(f"Extracting {outputvar} from output file.")
        return self.outputfile.get_output_var(outputvar)

    def _get_files_file(self, **kwargs):
        return search_in_all_subdirs(self.path, fileending=".files",
                                     expected=1, **kwargs)[0]

    def _get_log_file(self, **kwargs):
        self._logger.info("Looking for log file. Assume 'log' ending and"
                          " hope for only one log file.")
        return search_in_all_subdirs(self.path, fileending="log",
                                     expected=1, **kwargs)[0]

    def _get_input_file(self, **kwargs):
        return search_in_all_subdirs(self.path, fileending=".in",
                                     expected=1, **kwargs)[0]

    def _set_main_directory(self, directory_name):
        self.path = directory_name

    @property
    def status(self):
        checker = StatusChecker(self.path, self._ignore)
        return checker.status

    @staticmethod
    def is_calculation_dir(directory):
        # check if this directory is a calculation directory
        # to see if it is one, check if an input file (.in) is present
        # in the directory. If yes, than it is considered a calculation dir
        if not os.path.isdir(directory):
            raise NotADirectoryError(f"{directory} is not a directory.")
        entries = os.listdir(directory)
        files = [x for x in entries if
                 os.path.isfile(os.path.join(directory, x))]
        for f in files:
            if f.endswith(".in"):
                # found an input file
                return True
        return False
