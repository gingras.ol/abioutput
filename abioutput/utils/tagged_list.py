from ..bases import BaseUtility


class TaggedList(BaseUtility):
    """List class with a tag system parallel to the index system.
    """
    _loggername = "TaggedList"

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.items = []
        self.tags = {}

    def __len__(self):
        return len(self.items)

    def append(self, item, tag=None):
        self.items.append(item)
        index = len(self.items) - 1
        if tag is None:
            self.tags.update({index: index})
        else:
            if tag in self.tags.keys():
                raise NameError(f"Tag {tag} already exists!")
            self.tags.update({tag: index})

    def remove(self, tag):
        # remove a curve by its tag or index
        if tag not in self.tags:
            # check if its an index
            try:
                tag = self.get_tag_from_index(tag)
            except KeyError:
                raise KeyError(f"Cannot remove: {tag} does not exists.")
        index = self.tags[tag]
        del self.items[index]
        self.tags.pop(tag)
        # also need to -1 all index higher than index
        for tagg, ind in self.tags.items():
            if ind > index:
                self.tags[tagg] -= 1
                # if tag was the same as its index, downgrade it also
                if tagg == ind:
                    self.change_tag(ind - 1, tagg)

    def change_tag(self, newtag, oldtag):
        """Change the tag of an item according to its old tag or index.
        """
        if oldtag not in self.tags:
            try:
                oldtag = self.get_tag_from_index(oldtag)
            except KeyError:
                raise KeyError(f"Cannot change tag because {oldtag} doesnt"
                               "exist!")
        index = self.tags.pop(oldtag)
        self.tags[newtag] = index

    def get_tag_from_index(self, index):
        for tag, value in self.tags.items():
            if value == index:
                return tag
        # if we are here, index does not exists
        raise KeyError(f"{index} is not a valid index!")

    def __getitem__(self, tag):
        if tag not in self.tags:
            # maybe a tagged index
            try:
                tag = self.get_tag_from_index(tag)
            except KeyError:
                raise KeyError(f"Cannot get item: {tag} doesnt exist.")
        return self.items[self.tags[tag]]

    def __setitem__(self, tag, item):
        if tag not in self.tags:
            # maybe a tagged index
            try:
                tag = self.get_tag_from_index(tag)
            except KeyError:
                raise KeyError(f"Cannot set item: {tag} doesnt exist.")
        self.items[self.tags[tag]] = item

    def __iter__(self):
        for item in self.items:
            yield item

    def __add__(self, obj):
        newlist = TaggedList()
        # setup the newlist from this list
        for tag in self.tags:
            newlist.append(self[tag], tag=tag)
        # check that all tags are different
        for tag, index in obj.tags.items():
            if tag in self.tags and tag != self.tags[tag]:
                # the second tag!=self... is to check that simple numbered tag
                # are ok. Only identic specified tags must be different.
                raise KeyError(f"Cannot add two tagged list because tag {tag}"
                               f" is in both lits...")
        for tag, index in obj.tags.items():
            # if tag is the same as the index, don't add it
            if tag == index:
                newlist.append(obj[tag])
            # else, set the tag
            else:
                newlist.append(obj[tag], tag=tag)
        return newlist
