from ..bases import BaseUtility
import abc
import os


class BaseParser(BaseUtility, abc.ABC):
    _expected_ending = None

    def __init__(self, path, **kwargs):
        super().__init__(**kwargs)
        self._path = None
        self.path = path
        self.data = self._extract_data()

    def _extract_data(self):
        with open(self.path, "r") as f:
            lines = f.readlines()
        lines = self._clean_lines(lines)
        return self._extract_data_from_lines(lines)

    @abc.abstractmethod
    def _clean_lines(self, lines):
        pass

    @abc.abstractmethod
    def _extract_data_from_lines(self, lines):
        pass

    @property
    def path(self):
        return self._path

    @path.setter
    def path(self, path):
        if not os.path.exists(path):
            raise FileNotFoundError(f"Nothing found here: {path}.")
        if not os.path.isfile(path):
            raise FileNotFoundError(f"Not a file: {path}.")
        if self._expected_ending is not None:
            if not path.endswith(self._expected_ending):
                self._logger.warning(f"file {path} has its extension different"
                                     f" from expected ({self._expected_ending}"
                                     ")")
        self._path = path


class BaseSubParser(BaseUtility, abc.ABC):
    """SubParser Base class.
    """
    subject = None
    trigger = None

    def __init__(self, lines, **kwargs):
        super().__init__(**kwargs)
        self._ending_relative_index = None
        # TODO: check that this is not necessary!
        self.lines = self.preprocess_lines(lines)
        self.data = self._extract_data_from_lines()

    @abc.abstractmethod
    def _extract_data_from_lines(self):
        pass

    @property
    def ending_relative_index(self):
        if hasattr(self, "_ending_relative_index"):
            if self._ending_relative_index is not None:
                return self._ending_relative_index
        # if we are here, there is a dev error
        raise ValueError("Dev Error: ending_relative_index must be set (%s)." %
                         self.subject)

    @ending_relative_index.setter
    def ending_relative_index(self, index):
        self._ending_relative_index = index

    @staticmethod
    def preprocess_lines(lines):
        # TODO: lines should already have been cleaned up. This is redundant!
        return [x.strip("\n").strip() for x in lines]


class SubParsersList:
    """Class that takes a list of subparsers and manages it.
    """
    def __init__(self, slist):
        self.subparsers = {trigger: subparser for trigger, subparser in
                           zip([s.trigger for s in slist], slist)}

    def items(self):
        return self.subparsers.items()

    def remove(self, parser):
        del self.subparsers[parser.trigger]

    def __len__(self):
        return len(self.subparsers)

    def __iter__(self):
        for subparser in self.subparsers.values():
            yield subparser


class DataFileParser(BaseParser, abc.ABC):
    def __getitem__(self, key):
        return self.data[key]


class BaseInputFileParser(DataFileParser, abc.ABC):
    """Base class for an input file parser.
    """
    _expected_ending = ".in"

    @property
    def input_variables(self):
        return self.data
