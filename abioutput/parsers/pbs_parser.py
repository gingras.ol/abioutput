from .bases import DataFileParser
from .routines import decompose_line


class PBSFileParser(DataFileParser):
    """Parser for a pbs file. The pbs file structure is assumed
    to be the same as the corresponding abilaunch pbs writer.
    """
    _loggername = "PBSFileParser"
    _expected_ending = ".sh"

    def _clean_lines(self, lines):
        # just remove \n and empty spaces
        return [l.strip().strip("\n").strip() for l in lines]

    def _extract_data_from_lines(self, lines):
        lines = [l.strip().strip("\n") for l in lines]
        # remove empty lines
        lines = [l for l in lines if l]
        # init data dict
        data = {"modules": []}
        to_remove = []
        for line in lines:
            # interpreter
            if line.startswith("#!"):
                data["interpreter"] = line.split("!")[-1]
                to_remove.append(line)
            # if line starts with a #PBS => PBS command
            elif line.startswith("#PBS"):
                s, i, f = decompose_line(line)
                to_remove.append(line)
                if "-N" in line:
                    # jobname
                    data["jobname"] = s[-1]
                elif "walltime" in line:
                    # PBS -l walltime=1:00:00
                    data["walltime"] = s[-1].split("=")[-1]
                elif "nodes" in line:
                    # PBS -l nodes=1:m24G:ppn=12
                    # m24G might not be there (optional)
                    split = s[-1].split("=")
                    # split = [nodes, 1:m24G:ppn, 12]
                    data["nodes"] = split[1]
                    if data["nodes"].endswith("ppn"):
                        data["nodes"] = data["nodes"][:-4]
                    data["ppn"] = split[-1]
            elif line.startswith("module"):
                # module load command here
                s, i, f = decompose_line(line)
                data["modules"].append(s[-1])
                to_remove.append(line)
        for line in to_remove:
            lines.remove(line)
        abinit_command = self._parse_abi_command(lines)
        data.update(abinit_command)
        return data

    def _parse_abi_command(self, lines):
        # parse the abinit command to get all the paths to executable
        # and input/output files for the abinit command
        command_line_index = None
        other_lines = []
        data = {"lines_before": [],
                "lines_after": []}
        for i, line in enumerate(lines):
            if "<" in line:
                command_line_index = i
                data["command_line"] = line
            elif line.startswith("MPIRUN"):
                # MPIRUN="mpirun -npernode=12"
                data["mpi_command"] = line.split('"')[1]
            elif line.startswith("EXECUTABLE"):
                data["command"] = line.split("=")[-1].strip('"')
            elif line.startswith("INPUT"):
                data["input_file_path"] = line.split("=")[-1].strip('"')
            elif line.startswith("LOG"):
                data["log_path"] = line.split("=")[-1].strip('"')
            elif line.startswith("STDERR"):
                data["stderr_path"] = line.split("=")[-1].strip('"')
            else:
                other_lines.append(i)
        if command_line_index is None:
            raise LookupError(f"Could not find executable command line call in"
                              f" {self.filename}.")
        for index in other_lines:
            if index < command_line_index:
                data["lines_before"].append(lines[index])
            else:
                data["lines_after"].append(lines[index])
        return data
