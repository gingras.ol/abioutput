from ..bases import DataFileParser
from .output_subparsers import DtsetParser, HeaderParser, FooterParser


class AbinitOutputParser(DataFileParser):
    """An ABINIT output file parser that gets data from an output file.

    Parameters
    ----------
    filepath : str
               The path to the output file.
    """
    _loggername = "OutputParser"
    _expected_ending = ".out"

    def _clean_lines(self, lines):
        return [l.strip("\n") for l in lines]

    def _extract_data_from_lines(self, lines):
        self._logger.debug("=== Parsing Output file ===")
        skip = 0
        dtsets_data = []
        header_data = None
        footer_data = None
        for index, line in enumerate(lines):
            if index < skip:
                continue
            if DtsetParser.trigger in line:
                # parse the dtset
                self._logger.debug(f"Found dtset trigger in {line}.")
                parser = DtsetParser(lines[index:],
                                     loglevel=self._logger.level)
                dtsets_data.append(parser.data)
                skip += parser.ending_relative_index
                continue
            if HeaderParser.trigger in line:
                # parse the header
                self._logger.debug(f"Found header trigger in {line}.")
                parser = HeaderParser(lines[index:],
                                      loglevel=self._logger.level)
                header_data = parser.data
                skip += parser.ending_relative_index
                continue
            if FooterParser.trigger in line:
                # parse the footer
                self._logger.debug(f"Found footer trigger in {line}.")
                parser = FooterParser(lines[index:],
                                      loglevel=self._logger.level)
                footer_data = parser.data
                skip += parser.ending_relative_index
                continue
        return {"dtsets": dtsets_data,
                "header": header_data,
                "footer": footer_data}

    def get_output_var(self, output_var_name):
        if output_var_name not in self.output_vars:
            return None
        return self.data["footer"][output_var_name]

    def get_input_var(self, input_var_name):
        if input_var_name not in self.input_vars:
            return None
        return self.data["header"][input_var_name]

    @property
    def output_vars(self):
        return self.data["footer"]

    @property
    def input_vars(self):
        return self.data["header"]

    @property
    def data_per_dtset(self):
        return self.data["dtsets"]
