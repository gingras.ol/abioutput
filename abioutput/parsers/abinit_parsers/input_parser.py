from ..bases import BaseInputFileParser
from .output_subparsers import VariableSubParser


class AbinitInputFileParser(BaseInputFileParser):
    """Parser for an abinit input file.
    """
    _loggername = "AbinitInputFileParser"

    def _clean_lines(self, lines):
        clean = []
        for line in lines:
            if line.startswith("#"):
                continue
            strip_nl = line.strip("\n")
            strip_spaces = strip_nl.strip(" ")
            if not strip_spaces:
                continue
            clean.append(strip_spaces)
        return clean

    def _extract_data_from_lines(self, lines):
        data = {}
        next_end = 0
        for i, line in enumerate(lines):
            if i < next_end:
                continue
            if VariableSubParser.variable_def_start_here(line):
                var = VariableSubParser(lines[i:],
                                        loglevel=self._logger.level)
                next_end += var.ending_relative_index
                data[var.name] = var.value
        return data
