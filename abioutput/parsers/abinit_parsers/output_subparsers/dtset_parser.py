from .eig_parser import EIGParser
from ...bases import BaseSubParser, SubParsersList


SUBPARSERS = (EIGParser, )


class DtsetParser(BaseSubParser):
    """Class that parses a dtset from an abinit output file.
    """
    _loggername = "DtsetParser"
    subject = "dtset"
    trigger = "== DATASET"

    def _extract_data_from_lines(self):
        self._logger.debug("=== Parsing DATASET ===")
        data = {}
        # extract data from dtset
        skip = 0
        subparsers = SubParsersList(SUBPARSERS)
        for index, line in enumerate(self.lines):
            if index < skip:
                # don't work on this line
                continue
            for trigger, subparser in subparsers.items():
                if trigger in line:
                    # this line is a trigger for the subparser to work
                    # parse the rest of the lines from here
                    s = subparser(self.lines[index:],
                                  loglevel=self._logger.level)
                    data[s.subject] = s.data
                    # remove the subparser from the list to not parse again
                    subparsers.remove(subparser)
                    # modify the skip to not reparse the parsed lines
                    skip = index + s.ending_relative_index
                    # stop parsing the active line
                    break
            if self.trigger in line or "== END DATASET(S)" in line:
                # dataset ends here
                self._ending_relative_index = index - 1
                break
        return data

    @classmethod
    def from_string(cls, string, **kwargs):
        """Parses a dtset from a single string instead of a list of lines.
        """
        lines = string.split("\n")
        return cls(lines, **kwargs)
