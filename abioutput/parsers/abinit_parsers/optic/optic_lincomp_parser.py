from ...bases import DataFileParser
from ...routines import decompose_line
import numpy as np


class OpticLincompParser(DataFileParser):
    """Parses an optic file containing data of a linear component of the
    dielectric tensor.
    """
    _loggername = "OpticLincompParser"

    def _extract_data_from_lines(self, lines):
        data = {}
        data.update(self._get_header_from_lines(lines))
        data.update(self._get_core_data(lines))
        return data

    def _clean_lines(self, lines):
        # nothing to do really
        return lines

    def _get_core_data(self, lines):
        # need to separate data into blocks
        impart = None
        repart = None
        abspart = None
        kappapart = None
        npart = None
        reflpart = None
        absorbpart = None
        for i, line in enumerate(lines):
            line = line.strip()
            if not line.startswith("#"):
                continue
            if "Im(eps(w))" in line:
                # start of imaginary part
                impart = i + 1
                continue
            elif "Re(eps(w))" in line:
                # start of real part
                repart = i + 1
                continue
            elif "abs(eps(w))" in line:
                abspart = i + 1
                continue
            elif "kappa" in line:
                kappapart = i + 1
                continue
            elif "Re(refractive" in line:
                npart = i + 1
                continue
            elif "Reflectivity" in line:
                reflpart = i + 1
                continue
            elif "absorption" in line:
                absorbpart = i + 1
                continue

        if impart is None:
            raise LookupError("Could not locate imaginary part of eps")
        if repart is None:
            raise LookupError("Could not locate real part of eps")
        if abspart is None:
            raise LookupError("Could not locate abs value of eps")
        if kappapart is None:
            raise LookupError("Could not locate Im part of refrac index")
        if npart is None:
            raise LookupError("Could not locate Re part of refrac index")
        if reflpart is None:
            raise LookupError("Could not locate reflectivity data")
        if absorbpart is None:
            raise LookupError("Could not locate absorption data")
        imaginary = self._extract_data_block(lines[impart:repart])
        real = self._extract_data_block(lines[repart:abspart])
        absolute = self._extract_data_block(lines[abspart:kappapart])
        im_refractive = self._extract_data_block(lines[kappapart:npart])
        re_refractive = self._extract_data_block(lines[npart:reflpart])
        reflectivity = self._extract_data_block(lines[reflpart:absorbpart])
        absorption = self._extract_data_block(lines[absorbpart:])
        return {"frequencies": imaginary[:, 0],
                "epsilon": real[:, 1] + 1.0j * imaginary[:, 1],
                "refractive index": (re_refractive[:, 1] + 1.0j *
                                     im_refractive[:, 1]),
                "absolute epsilon": absolute[:, 1],
                "reflectivity": reflectivity[:, 1],
                "absorption": absorption[:, 1]}

    def _extract_data_block(self, lines):
        # lines here should be a list of strings with two float values
        data = []
        for line in lines:
            if not line or "#" in line:
                # don't consider empty lines or comments
                continue
            s, i, f = decompose_line(line)
            if len(f) != 2:
                continue
            data.append(np.array(f))
        return np.array(data)

    def _get_header_from_lines(self, lines):
        comp = self._get_component(lines[0])
        broadening = self._get_broadening(lines[1])
        scissor = self._get_scissor(lines[2])
        window = self._get_window(lines[3])
        return {"component": comp,
                "broadening": broadening,
                "scissor shift": scissor,
                "window": window}

    def _get_window(self, line):
        # line should look like this
        # #energy window:    2.790557E+01eV    1.025511E+00Ha
        s, i, f = decompose_line(line)
        # return the float with the 'eV' string in it
        for string in s:
            if "eV" in string:
                return float(string.strip("eV"))
        raise LookupError(f"Could not extract energy window from {line}")

    def _get_scissor(self, line):
        # line should look like this
        # #scissors shift:    4.191000E-02
        s, i, f = decompose_line(line)
        if not f:
            raise LookupError(f"Could not extract scissor shift from {line}")
        return f[0]

    def _get_broadening(self, line):
        # line look like this:
        # #broadening:    0.000000E+00    1.000000E-03
        s, i, f = decompose_line(line)
        if len(f) != 2:
            raise LookupError(f"Could not extract broadening from {line}")
        return f[-1]

    def _get_component(self, line):
        # first line look like this:
        # #calculated the component:  1  1  of dielectric function
        s, i, f = decompose_line(line)
        if len(i) != 2:
            raise LookupError(f"Could not parse the tensor component from:"
                              f" {line}")
        return str(i[0]) + str(i[1])
