from ..bases import BaseFilesFileParser


class OpticFilesFileParser(BaseFilesFileParser):
    """Parser for the optic files file.
    """
    _loggername = "OpticFilesFileParser"

    def _extract_data_from_lines(self, lines):
        data = {}
        data["input_path"] = lines[0]
        data["output_path"] = lines[1]
        data["output_data_prefix"] = lines[2]
        return data
