from .bases import BaseFilesFileParser


class AbinitFilesFileParser(BaseFilesFileParser):
    """Parser that parses a files file.
    """
    _loggername = "FilesFileParser"

    def _extract_data_from_lines(self, lines):
        data = {}
        data["input_path"] = lines[0]
        data["output_path"] = lines[1]
        data["input_data_prefix"] = lines[2]
        data["output_data_prefix"] = lines[3]
        data["tmp_data_prefix"] = lines[4]
        data["pseudos"] = [x for x in lines[5:]]
        return data
