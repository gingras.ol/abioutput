from ..bases import DataFileParser
import abc


class BaseFilesFileParser(DataFileParser, abc.ABC):
    """Base class for files file parsers.
    """
    _expected_ending = ".files"

    def _clean_lines(self, lines):
        lines = [l.strip().strip("\n") for l in lines]
        lines = [l for l in lines if not (l.startswith("#") or len(l) == 0)]
        return lines
