from .output_parser import AbinitOutputParser


class AbinitLogParser(AbinitOutputParser):
    """An ABINIT log file parser.
    """
    _expected_ending = ".log"

    def _get_data_per_dtset(self, *args, **kwargs):
        """Overridden function. For a log file, do not parse each dtset for
        now.

        TODO: Implement this function for dtset parsing in the logfile.
        """
        return None
