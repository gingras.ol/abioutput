from ...bases import BaseSubParser
from ...routines import decompose_line


class ParallelizationSubParser(BaseSubParser):
    """Subparser for the parallelization info section.
    """
    _loggername = "ParallelizationSubParser"
    subject = "parallelization info"
    trigger = "Parallelization info"

    def _extract_data_from_lines(self):
        data = {}
        for j, line in enumerate(self.lines):
            if "Sum" in line:
                s, i, f = decompose_line(line)
                data["pw"] = i[-1]
                self.ending_relative_index = j
                break
        return data


class EnergySubParser(BaseSubParser):
    """Subparser for the energy info.
    """
    _loggername = "EnergySubParser"
    subject = "total energy"  # extract only one energy for now
    trigger = "!"

    def _extract_data_from_lines(self):
        # only one line for now
        self.ending_relative_index = 0
        s, i, f = decompose_line(self.lines[0])
        return f[0]


class InputParametersSubParser(BaseSubParser):
    """SubParser for some input parameters.
    """
    _loggername = "InputParametersSubParser"
    subject = "inputs"
    trigger = "bravais-lattice"

    def _extract_data_from_lines(self):
        data = {}
        for j, line in enumerate(self.lines):
            if "kinetic-energy cutoff" in line:
                s, i, f = decompose_line(line)
                data["ecutwfc"] = f[0]
                continue
            if "number of atoms/cell" in line:
                s, i, f = decompose_line(line)
                data["nat"] = i[0]
                continue
            if "celldm(1)" in line or "celldm(4)" in line:
                # next 2 lines are celldm e.g.:
                # celldm(1)= 20.000000 celldm(2)= 0.000000 celldm(3)= 0.000000
                s, i, f = decompose_line(line)
                celldms = [string.split("=")[0] for string in s]
                for celldm, val in zip(celldms, f):
                    data[celldm] = val
                continue
            if "volume" in line:
                s, i, f = decompose_line(line)
                data["volume"] = f[0]
                continue
            if "PseudoPot" in line:
                # stop here
                self.ending_relative_index = j
                return data
        raise LookupError("Couldn't find end of section.")


class TimeSubParser(BaseSubParser):
    """Subparser that parses time values.
    """
    _loggername = "TimeSubParser"
    subject = "timing"
    trigger = "PWSCF        :"

    def _extract_data_from_lines(self):
        data = {}
        # only one line
        # e.g.: PWSCF        :    1m 3.35s CPU       1m  3.62s WALL
        # if < 1m: minutes don't appear
        s, i, f = decompose_line(self.lines[0])
        s = s[2:-1]  # remove PWSCF, : and WALL
        s.remove("CPU")  # remove CPU now there should only be time measures
        # group cputime (first group)
        cputime = []
        for index, item in enumerate(s):
            cputime.append(item)
            if "s" in item:
                s = s[index + 1:]
                break
        # join string here cause sometimes, QE print them already joined
        data["walltime"] = float(self._convert_str_to_sec("".join(s)))
        data["cputime"] = float(self._convert_str_to_sec("".join(cputime)))
        self.ending_relative_index = 0
        return data

    def _convert_str_to_sec(self, string):
        # e.g.: 1m11.81s
        # assume nothing more complicated
        rm_sec = string.strip("s")
        split_min = rm_sec.split("m")
        try:
            nsecs = float(split_min[-1])
        except ValueError:
            nsecs = 0
        split_hr = split_min[0].split("h")
        try:
            nmins = float(split_hr[-1])
        except ValueError:
            nmins = 0
        try:
            nhours = float(split_hr[0])
        except ValueError:
            nhours = 0
        return nsecs + nmins * 60 + nhours * 3600


class KGridSubParser(BaseSubParser):
    """Subparser that parses kgrids information.
    """
    _loggername = "KGridSubparser"
    subject = "k-points"
    trigger = "number of k points"

    def _extract_data_from_lines(self):
        # for now only one data to extract
        self.ending_relative_index = 0
        s, i, f = decompose_line(self.lines[0])
        return i[0]


class AtomicPositionsSubParser(BaseSubParser):
    """Subparser that parses atomic positions.
    """
    _loggername = "AtomicPositionsSubParser"
    subject = "atomic positions"
    trigger = "Cartesian axes"

    def _extract_data_from_lines(self):
        data = {}
        for index, line in enumerate(self.lines):
            if "number of k points" in line:
                # reached the end
                self.ending_relative_index = index - 1
                return data
            if "tau" in line:
                atom, position = self._get_atom_pos(line)
                if atom not in data:
                    data[atom] = []
                data[atom].append(position)
                continue

    def _get_atom_pos(self, line):
        # line look like this:
        # 1           Cl  tau(   1) = (   0.0000000   0.0000000   0.0000000  )
        s, i, f = decompose_line(line)
        atom = s[0]
        pos = f[-3:]
        return atom, pos
