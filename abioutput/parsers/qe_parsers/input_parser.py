from ..bases import BaseInputFileParser
from ..routines import decompose_line


class QEInputFileParser(BaseInputFileParser):
    """Class that can parse a Quantum Espresso Input File.
    """
    _loggername = "QEInputFileParser"

    def _clean_lines(self, lines):
        clean = []
        for line in lines:
            if line.startswith("&") or line.startswith("/"):
                continue
            strip_nl = line.strip("\n")
            strip_spaces = strip_nl.strip(" ")
            if not strip_spaces:
                continue
            clean.append(strip_spaces)
        return clean

    def _extract_data_from_lines(self, lines):
        data = {}
        next_end = 0
        for i, line in enumerate(lines):
            if i < next_end:
                continue
            if "=" in line:
                # a variable is defined here
                # e.g.: calculation = 'scf',
                strip_comma = line.strip(",")
                split = strip_comma.split("=")
                split = [s.strip(" ") for s in split]
                varname = split[0]
                varvalue = split[1]
                if "'" in varvalue or '"' in varvalue:
                    # value is a string
                    data[varname] = varvalue.strip("\'\"")
                    continue
                # check if it is an integer or float
                try:
                    data[varname] = int(varvalue)
                except ValueError:
                    # not an integer
                    pass
                else:
                    continue
                try:
                    data[varname] = float(varvalue)
                except ValueError:
                    # not a float
                    pass
                else:
                    continue
                # if we are here: what type is this?????
                raise LookupError(f"Could not determine var type of {line}...")
            elif "atomic_species" in line.lower():
                self._logger.debug("Found ATOMIC_SPECIES varblock.")
                # atomic species: the following lines are a list of atom types
                (at_spec,
                 next_end_rel) = self._extract_atomic_species(lines[i + 1:])
                next_end = i + next_end_rel + 1
                data["atomic_species"] = at_spec
                continue
            elif "atomic_positions" in line.lower():
                self._logger.debug("Found ATOMIC_POSITIONS varblock.")
                parameter = line.split(" ")[-1]
                (at_pos,
                 next_end_rel) = self._extract_atomic_positions(lines[i + 1:])
                next_end = i + next_end_rel + 1
                data["atomic_positions"] = {"parameter": parameter,
                                            "positions": at_pos}
                continue
            elif "k_points" in line.lower():
                self._logger.debug("Found K_POINTS varblock.")
                parameter = line.split(" ")[-1]
                k_pts, next_end_rel = self._extract_kpts(lines[i + 1:],
                                                         parameter)
                next_end = i + next_end_rel + 1
                data["k_points"] = {parameter: k_pts}
                continue
        return data

    def _extract_kpts(self, lines, parameter):
        # lines should be starting with the list of kpts parameters
        if parameter != "automatic":
            raise NotImplementedError()
        data = None
        for i, line in enumerate(lines):
            if "atomic" in line.lower():
                return data, i
            s, i, f = decompose_line(line)
            data = i
        # end of file
        return data, len(lines)

    def _extract_atomic_positions(self, lines):
        # lines should be starting with a list of atomic positions
        data = {}
        for j, line in enumerate(lines):
            if "atomic" in line.lower() or "k_points" in line.lower():
                # Reached new section => return data gathered
                return data, j
            s, i, f = decompose_line(line)
            atom_type = s[0]
            coordinates = f
            if atom_type not in data:
                data[atom_type] = [coordinates]
            else:
                data[atom_type].append(coordinates)
        # reached end of file, return everything
        return data, len(lines)

    def _extract_atomic_species(self, lines):
        # lines should be starting with a list of atomic species
        data = []
        for j, line in enumerate(lines):
            if "atomic" in line.lower() or "k_points" in line.lower():
                # reached new section => return data gathered
                return data, j
            s, i, f = decompose_line(line)
            atom = s[0]
            atomic_mass = f[0]
            pseudo = s[1]
            data.append({"atom": atom,
                         "atomic_mass": atomic_mass,
                         "pseudo": pseudo})
        # reached end of file
        return data, len(lines)
