from ..bases import DataFileParser


class QEDataFileParser(DataFileParser):
    """Just a base class for Quantum Espresso data file parsers.
    """

    def _clean_lines(self, lines):
        return [l.strip().strip("\n").strip() for l in lines]
