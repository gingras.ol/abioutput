from ..bases import SubParsersList
from .bases import QEDataFileParser
from .subparsers.log_subparsers import (ParallelizationSubParser,
                                        EnergySubParser,
                                        InputParametersSubParser,
                                        TimeSubParser, KGridSubParser,
                                        AtomicPositionsSubParser)


SUBPARSERS = (ParallelizationSubParser, EnergySubParser,
              InputParametersSubParser, TimeSubParser, KGridSubParser,
              AtomicPositionsSubParser)


# TODO: Rebase this class with the DtsetParser class
class QELogParser(QEDataFileParser):
    """A Quantum Espresso log file parser that extracts data from a
    log file.
    """
    _loggername = "QELogParser"
    _expected_ending = ".log"

    def _extract_data_from_lines(self, lines):
        data = {}
        skip = 0
        subparsers = SubParsersList(SUBPARSERS)
        for i, line in enumerate(lines):
            if i < skip:
                continue
            for trigger, subparser in subparsers.items():
                if trigger in line:
                    s = subparser(lines[i:],
                                  loglevel=self._logger.level)
                    data[s.subject] = s.data
                    subparsers.remove(subparser)
                    skip = i + s.ending_relative_index
                    break
        if len(subparsers):
            # couldnt get some information
            missed = [s.subject for s in subparsers]
            self._logger.warning(f"Some info couldn't be gathered: {missed}")
        return data
