from .bases import QEDataFileParser
from ..routines import decompose_line


class QEFreqParser(QEDataFileParser):
    """A Quantum Espresso .freq file parser that extracts data from
    a .freq file.
    """
    _loggername = "QEFreqParser"
    _expected_ending = ".freq"

    def _extract_data_from_lines(self, lines):
        data = {}
        eigs = []
        kpts = []
        # data in first line
        # &plot nbnd=   6, nks=  41 /
        # but first need to split by ','
        split0 = lines[0].split(",")
        s, i, f = decompose_line(split0[0])
        data["nbands"] = i[0]
        s, i, f = decompose_line(split0[1])
        data["npts"] = i[0]
        for j, line in enumerate(lines[1:]):
            # data alternates between coordinates and energies
            s, i, f = decompose_line(line)
            if len(f) == 3:
                # coordinate line
                kpts.append(f)
                continue
            # else we have an energy line
            # WE ASSUME HERE THAT ENERGIES ARE WRITTEN ON A SINGLE LINE
            eigs.append(f)
        # make some checks
        assert len(eigs) == len(kpts)
        assert len(eigs) == data["npts"]
        assert len(eigs[0]) == data["nbands"]
        data["coordinates"] = kpts
        data["eigenvalues"] = eigs
        return data
