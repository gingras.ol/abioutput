from abilaunch.writers import (AbinitInputFileWriter,
                               AbinitFilesFileWriter,
                               OpticFilesFileWriter,
                               OpticInputFileWriter)
from abioutput.parsers import (AbinitInputFileParser, AbinitOutputParser,
                               AbinitFilesFileParser,
                               DMFTProjectorsParser, DMFTEigParser,
                               OpticFilesFileParser, OpticInputFileParser,
                               OpticLincompParser)
from .bases import BaseParserTest, BaseInputFileParserTest
from ..variables_for_tests import (abinit_out_example, abinit_vars,
                                   optic_lincomp_example,
                                   dmft_projectors_example,
                                   dmft_hamiltonian_example)
import os
import tempfile
import unittest


class TestOutputParser(BaseParserTest, unittest.TestCase):
    _example_file = abinit_out_example
    _parser = AbinitOutputParser

    def test_output_parser(self):
        # in this example file, it is just a GS calculation and all the input
        # variables are fixed => they do not change after the calculation
        # they should be the same as the abinit_vars dict
        for name, value in abinit_vars.items():
            self.assertIn(name, self.parser.input_vars)
            self.assertEqual(value, self.parser.get_input_var(name))
            self.assertIn(name, self.parser.output_vars)
            self.assertEqual(value, self.parser.get_output_var(name))


class TestOpticInputFileParser(BaseInputFileParserTest, unittest.TestCase):
    _inputvars = {"ddkfile_1": "ddk1",
                  "ddkfile_2": "ddk2",
                  "ddkfile_3": "ddk3",
                  "wfkfile": "WFK",
                  "broadening": 0.002,
                  "domega": 0.0003,
                  "maxomega": 0.3,
                  "scissor": 0.0,
                  "tolerance": 0.002,
                  "num_lin_comp": 1,
                  "lin_comp": 11,
                  "num_nonlin_comp": 2,
                  "nonlin_comp": [123, 222],
                  "num_linel_comp": 0,
                  "num_nonlin2_comp": 0}
    _parser = OpticInputFileParser
    _writer = OpticInputFileWriter


class TestOpticLincompParser(BaseParserTest, unittest.TestCase):
    _example_file = optic_lincomp_example
    _parser = OpticLincompParser


class TestAbinitInputFileParser(BaseInputFileParserTest, unittest.TestCase):
    _writer = AbinitInputFileWriter
    _parser = AbinitInputFileParser
    _inputvars = abinit_vars


class BaseFilesFileParserTest:
    _parser = None
    _writer = None

    def setUp(self):
        self.tempdir = tempfile.TemporaryDirectory()
        self.path = os.path.join(self.tempdir.name,
                                 "test_files_file_parser.files")
        self.writer = self._writer()
        self.writer.path = self.path
        # all these settings are not mandatory for certain files file writers
        # (e.g.: Optic files file don't need input_data_prefix)
        self.writer.input_file_path = "test_files_file_parser.in"
        self.writer.output_file_path = "test_files_file_parser.out"
        self.writer.output_data_prefix = "odat_test_files_file_parser"
        self.writer.output_data_directory = "."
        self.writer.input_data_prefix = "idat_test_files_file_parser"
        self.writer.tmp_data_prefix = "tmp_test"
        self.writer.pseudos = ["pseudo1", "pseudo2"]
        self.writer.write()

    def test_files_file_parser(self):
        # check that it has written everything it had to
        self.parser = self._parser(self.path)
        self.assertEqual(self.parser.data["input_path"],
                         self.writer.input_file_path)
        self.assertEqual(self.parser.data["output_path"],
                         self.writer.output_file_path)
        self.assertEqual(self.parser.data["output_data_prefix"],
                         self.writer.output_data_prefix)

    def tearDown(self):
        self.tempdir.cleanup()
        del self.tempdir


class TestOpticFilesFileParser(BaseFilesFileParserTest, unittest.TestCase):
    _parser = OpticFilesFileParser
    _writer = OpticFilesFileWriter


class TestAbinitFilesFileParser(BaseFilesFileParserTest, unittest.TestCase):
    _parser = AbinitFilesFileParser
    _writer = AbinitFilesFileWriter

    def test_files_file_parser(self):
        BaseFilesFileParserTest.test_files_file_parser(self)
        self.assertEqual(self.parser.data["input_data_prefix"],
                         self.writer.input_data_prefix)
        self.assertEqual(self.parser.data["tmp_data_prefix"],
                         self.writer.tmp_data_prefix)
        self.assertListEqual(self.parser.data["pseudos"],
                             self.writer.pseudos)


class TestDMFTProjectorsParser(BaseParserTest, unittest.TestCase):
    _parser = DMFTProjectorsParser
    _example_file = dmft_projectors_example


class TestDMFTEigParser(BaseParserTest, unittest.TestCase):
    _parser = DMFTEigParser
    _example_file = dmft_hamiltonian_example
