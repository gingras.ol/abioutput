from abilaunch.writers import PBSFileWriter
from abioutput.parsers import PBSFileParser
import os
import tempfile
import unittest


class TestPBSFileParser(unittest.TestCase):
    def setUp(self):
        self.tempdir = tempfile.TemporaryDirectory()
        self.path = os.path.join(self.tempdir.name, "test_pbs_parser.sh")
        self.writer = PBSFileWriter(allow_cmd_invisible=True)
        self.writer.path = self.path
        self.writer.input_file_path = "test.files"
        self.writer.log_path = "test.log"
        self.writer.stderr_path = "test.stderr"
        self.writer.jobname = "test_pbs_parser"
        self.writer.walltime = "1:00:00"
        self.writer.nodes = "1:m24G"
        self.writer.ppn = 12
        self.writer.mpi_command = "mpirun -npernode 12"
        self.writer.command = "abinit"
        self.writer.modules = ["MPI/Gnu/gcc4.9.2/openmpi/1.8.8"]
        self.writer.lines_before = "cd $PBS_O_WORKDIR"
        self.writer.write(force_queuing_system="torque")

    def test_pbs_file_parser(self):
        parser = PBSFileParser(self.path)
        self.assertEqual(parser["input_file_path"],
                         self.writer.input_file_path)
        self.assertEqual(parser["log_path"], self.writer.log_path)
        self.assertEqual(parser["stderr_path"], self.writer.stderr_path)
        self.assertEqual(parser["walltime"], self.writer.walltime)
        self.assertEqual(parser["nodes"], self.writer.nodes)
        self.assertEqual(parser["jobname"], self.writer.jobname)
        self.assertEqual(int(parser["ppn"]), self.writer.ppn)
        self.assertEqual(parser["mpi_command"], self.writer.mpi_command)
        self.assertEqual(parser["command"], self.writer.command)
        self.assertListEqual(parser["lines_before"], self.writer.lines_before)
        self.assertListEqual(parser["lines_after"], self.writer.lines_after)
        self.assertListEqual(parser["modules"], self.writer.modules)
