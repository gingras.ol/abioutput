import os
import tempfile


class BaseParserTest:
    _example_file = None
    _parser = None

    def setUp(self):
        self.parser = self._parser(self._example_file)

    def test_no_errors(self):
        # nothing to do just make sure there is no errors
        self.assertEqual(self._example_file, self.parser.path)


class BaseInputFileParserTest:
    _writer = None
    _parser = None
    _inputvars = None

    def setUp(self):
        self.tempdir = tempfile.TemporaryDirectory()
        self.path = os.path.join(self.tempdir.name, "test_input_parser.in")
        self.writer = self._writer()
        self.writer.path = self.path
        self.writer.input_variables = self._inputvars
        self.writer.write()

    def tearDown(self):
        self.tempdir.cleanup()
        del self.tempdir

    def test_input_parser(self):
        parser = self._parser(self.path)
        self.assertDictEqual(parser.input_variables, self._inputvars)
