from abioutput.parsers import QEInputFileParser, QELogParser
from abilaunch.writers import QEPWInputFileWriter
from .bases import BaseParserTest, BaseInputFileParserTest
from ..variables_for_tests import qe_pwxlog_example, qe_pwx_vars
import unittest


class TestQELogParser(BaseParserTest, unittest.TestCase):
    _example_file = qe_pwxlog_example
    _parser = QELogParser


class TestQEInputFileParser(BaseInputFileParserTest, unittest.TestCase):
    _writer = QEPWInputFileWriter
    _parser = QEInputFileParser
    _inputvars = qe_pwx_vars
