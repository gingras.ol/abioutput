import os
import tempfile
import unittest
from abilaunch import AbinitMassLauncher
from abioutput.utils import TreeBuilder
# tbase1_1 input variables
from .variables_for_tests import abinit_vars


here = os.path.dirname(os.path.abspath(__file__))
pseudo = os.path.join(here, "../../examples/pseudos/01h.pspgth")
# remove ecut from input vars
abinit_vars = abinit_vars.copy()
abinit_vars.pop("ecut")


class TestTreeBuilder(unittest.TestCase):
    jobnames = ["ecut5", "ecut10"]

    def setUp(self):
        self.tempdir = tempfile.TemporaryDirectory()
        self.path = self.tempdir.name
        # need to mock the abinit script
        self.abinit = tempfile.NamedTemporaryFile()
        self.launcher = AbinitMassLauncher(self.jobnames)
        self.launcher.workdir = self.path
        self.launcher.common_pseudos = pseudo
        self.launcher.common_input_variables = abinit_vars
        self.launcher.specific_input_variables = [{"ecut": 5.0},
                                                  {"ecut": 10.0}]
        self.launcher.command = self.abinit.name
        self.launcher.write()
        self.tree_builder = TreeBuilder(self.path, loglevel=1)

    def tearDown(self):
        self.abinit.close()
        self.tempdir.cleanup()
        del self.launcher
        del self.tempdir

    def test_status_check(self):
        statuses = self.tree_builder.status
        for status in statuses:
            self.assertTrue(status["calculation_started"] is False)
            self.assertTrue(status["calculation_finished"] is False)

    def test_print_status(self):
        # test that printing status does not raise an error
        self.tree_builder.print_status()

    def test_print_ecut_convergence(self):
        # test that printing the ecut convergence does not raise an error
        self.tree_builder.print_attributes("status", "convergence_reached",
                                           "ecut", "etotal",
                                           delta="etotal",
                                           sortby="ecut")
        # test that forgetting the sortby, it raises an error
        with self.assertRaises(ValueError):
            self.tree_builder.print_attributes("status", "convergence_reached",
                                               "ecut", "etotal",
                                               delta="etotal")
