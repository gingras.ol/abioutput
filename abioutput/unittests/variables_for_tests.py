import os


here = os.path.dirname(os.path.abspath(__file__))

# abinit tbase1_1 input variables
abinit_vars = {"acell": [10, 10, 10],
               "ntypat": 1,
               "znucl": 1,
               "natom": 2,
               "typat": [1, 1],
               "xcart": [[-0.7, 0.0, 0.0], [0.7, 0.0, 0.0]],
               "ecut": 10.0,
               "kptopt": 0,
               "nkpt": 1,
               "nstep": 10,
               "toldfe": 1.0e-6,
               "diemac": 2.0,
               "optforces": 1}

# abinit output file example
abinit_out_example = os.path.join(here,
                                  ("../../examples/parsers/abinit_parsers/"
                                   "output_parser/example.out"))
# optic lincomp file example
optic_lincomp_example = os.path.join(here,
                                     ("../../examples/parsers/abinit_parsers/"
                                      "optic_lin_comp_parser/optic_lin_comp_"
                                      "example.out"))
# DMFT file examples
dmft_projectors_example = os.path.join(here,
                                       ("../../examples/parsers/"
                                        "abinit_parsers/"
                                        "dmft_parsers/projectors/"
                                        "projectors_example.ovlp"))
dmft_hamiltonian_example = os.path.join(here,
                                        ("../../examples/parsers/"
                                         "abinit_parsers/"
                                         "dmft_parsers/hamiltonian/"
                                         "forlb.eig"))
# Quantum Espresso log file example
qe_pwxlog_example = os.path.join(here, ("../../examples/parsers/qe_parsers/"
                                        "log_parser/example.log"))
# very simple QE example
qe_pwx_vars = {"calculation": "scf",
               "prefix": "silicon",
               "pseudo_dir": "./",
               "outdir": "./",
               "ibrav": 2,
               "celldm(1)": 10.28,
               "nat": 2,
               "ntyp": 1,
               "ecutwfc": 18.0,
               "atomic_species": [{"atom": "Si", "atomic_mass": 28.086,
                                   "pseudo": "Si.pz-vbc.UPF"}],
               "atomic_positions": {"parameter": "alat",
                                    "positions": {"Si": [[0.00, 0.00, 0.00],
                                                         [0.25, 0.25, 0.25]],
                                                  }},
               "k_points": {"automatic": [4, 4, 4, 1, 1, 1]}}
