import numpy as np


def sort_data(tosort, *args):
    """Sort all args according to the first array.

    Parameters
    ----------
    tosort : array
             The array to sort. All other arrays will be sorted the
             same as this one.
    """
    # check all arrays has the same len
    length = len(tosort)
    for array in args:
        if len(array) != length:
            raise ValueError("Arrays should have the same length.")
    sorted_args = np.array([x for _, *x in sorted(zip(tosort, *args))])
    sorted_args = [arr for arr in sorted_args.T]
    tosort = sorted(tosort)
    return [tosort] + sorted_args
