Welcome to Abioutput!
=====================

.. image:: https://gitlab.com/fgoudreault/abioutput/badges/master/build.svg
   :target: https://gitlab.com/fgoudreault/abioutput/pipelines/
.. image:: https://gitlab.com/fgoudreault/abioutput/badges/master/coverage.svg
   :target: https://gitlab.com/fgoudreault/abioutput/pipelines/

Installation
------------

Clone the repository::

  git clone git@github.com:fgoudreault/abioutput.git
  cd abioutput

Install using pip and install required packages::

  pip install -r requirements.txt
  pip install .

For a development installation, use the '-e' flag in 'pip install' and
(recommended) install optional packages for testing::

  pip install -r requirements.txt
  pip install -r requirements_optional.txt
  pip install -e .

For testing you also need the 
`abilaunch <https://gitlab.com/fgoudreault/abilaunch>`__. 
To install it, clone it in another directory::

  git clone git@gitlab.com:fgoudreault/abilaunch.git
  cd abilaunch
  pip install -r requirements.txt
  pip install -r requirements_optional.txt
  pip install .

To launch the test suite, just use the pytest command in the abioutput 
project directory::

  pytest


Usage
-----

You can find examples of how to run this project in the 'examples' directory.
